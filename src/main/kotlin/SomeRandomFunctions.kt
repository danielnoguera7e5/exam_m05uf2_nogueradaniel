package cat.itb.m05.uf2

//input m=3, n=7 output 3 4 5 6 7
/**
 * Es crea una llista dels nombres que hi ha entre els dos escollits.
 * @param[firstNumber] El primer nombre que s'introduirà en la llista.
 * @param[lastNumber] L'últim nombre que s'introduirà en la llista.
 * @return Llista de tots els nombres que hi ha entre el FirstValue i lastValue incloent-los.
 */
fun numbersBetweenValues(firstNumber: Int, lastNumber: Int): List<Int> {
    val numberList = mutableListOf<Int>()
    for (number in firstNumber..lastNumber) {
        numberList.add(number)
    }
    return numberList
}

/**
 * Retorna el valor més baix d'una llista d'Ints.
 * @param[numberList] Llista de nombres.
 * @return Nombre més baix de la llista de nombres.
 */
fun minValue(numberList: List<Int>): Int {
    var minValue = 0
    for (number in numberList.indices) {
        if (number < minValue) {
            minValue = number
        } else if (number == minValue) {
            minValue = number
        }
    }
    return minValue
}

/**
 * Retorna un String que es referrirà a una nota segons el nombre entrat.
 * @param[i] Nota de l'alumne.
 * @return Resultat en String de la Nota.
 */
// S'esepra el seguent comportament
//  0: No presentat
//  1-4: Insuficient
//  5-6: Suficient
//  7-8: Notable
//  9: Excel·lent
//  10: Excel·lent + MH
fun gradeToString(grade: Int): String {
    return when (grade) {
        0 -> "No presentat"
        1, 2, 3, 4 -> "Insuficient"
        5, 6 -> "Suficient"
        7, 8 -> "Notable"
        9, 10 -> "Excelent + MH"
        else -> "No valid"
    }
}

/**
 * Retorna les posicions dels caracters de -string- quan el caràcter és igual a -char-.
 * @param[string] String
 * @param[char] Caràcter del que volem saber les seves posicions a -string-
 */
// De tota la paraula
fun charPositionsInString(string: String, char: Char): List<Int> {
    val listOfPositions = mutableListOf<Int>()
    var coincidencies = 0
    for (stringChar in 0 until string.lastIndex) {
        if (string[stringChar] == char) {
            listOfPositions.add(stringChar)
        }
    }
    return listOfPositions
}

/**
 * Troba la primera posició de la lletra -char- a -String- i si no troba cap coincidencia retorna -1.
 * @param[string] String
 * @param[char] Caràcter del que volem saber la posició de la seva primera aparició a -string-
 * @return Retorna la posició de la primera aparició de -char- a -string-.
 */
//abc="marieta" b='a' -> 1
//abc="marieta" b='b' -> -1
fun firstOccurrencePosition(string: String, char: Char): Int {
    val position: MutableList<Int> = mutableListOf()
    for (i in 0 until string.length) {
        if (string[i] == char) {
            return i
        }
    }
    return -1
}

/**
 * Arrodoneix cap al nombre de menys valor si té menys que un 5. Si té més que 5 arrodoneix cap a dalt.
 * @param[grade] Nota de l'estudiant que volem arrodonir.
 * @return Nota arrodonida segons el -grade-
 */
/*
La funció ha de retornar la nota arredonida. 8.7 -> 9, 7.5 -> 8
Excepció, Si la nota no arriba a un 5, no obtindrà el 5. És a dir: 4.9 -> 4
 */
fun roundOff(grade: Double): Int {
    return grade.toInt()
}