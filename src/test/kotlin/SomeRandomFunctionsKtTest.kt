import cat.itb.m05.uf2.*
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SomeRandomFunctionsKtTest {

    @Test
    fun numbersBetweenValuesTest() {
        val actual = numbersBetweenValues(5,10)
        val list = listOf<Int>(5,6,7,8,9,10)
        assertEquals(list, actual)
    }

    @Test
    fun numbersNotBetweenValuesTest() {
        val actual = numbersBetweenValues(5,10)
        val list = listOf<Int>(5,6,7,8,9,10,11,12)
        assertEquals(list, actual)
    }

    @Test
    fun numbersBetweenValuesTestButBackwards() {
        val actual = numbersBetweenValues(10,5)
        val list = listOf<Int>(5,6,7,8,9,10)
        assertEquals(list, actual)
    }

    @Test
    fun numbersBetweenValuesTestButListIsBackwards() {
        val actual = numbersBetweenValues(10,5)
        val list = listOf<Int>(10,9,8,7,6,5)
        assertEquals(list, actual)
    }

    @Test
    fun minValue() {
        val listOfNumbers = listOf(7,4,9,22,5,3,8)
        val actual = minValue(listOfNumbers)
        assertEquals(3, actual)
    }

    @Test
    fun minValueIsZero() {
        val listOfNumbers = listOf(7,4,9,5,0,7,3,8)
        val actual = minValue(listOfNumbers)
        assertEquals(0, actual)
    }

    @Test
    fun gradeToStringTest() {
        val grade = 0
        val gradeToString = gradeToString(grade)
        assertEquals("No presentat", gradeToString)
    }

    @Test
    fun gradeToDifferentString() {
        val grade = 0
        val gradeToString = gradeToString(grade)
        assertEquals("Insuficient", gradeToString)
    }

    @Test
    fun differentGradeToString() {
        val grade = 8
        val gradeToString = gradeToString(grade)
        assertEquals("Insuficient", gradeToString)
    }

    @Test
    fun GradeIsNotValid() {
        val grade = 23
        val gradeToString = gradeToString(grade)
        assertEquals("No valid", gradeToString)
    }


    @Test
    fun charPositionsInStringTest() {
        val string = "provaaa"
        val char = 'a'
        val charPositionsInString = charPositionsInString(string,char)
        val toBeExpected = listOf<Int>(4,5,6)
        assertEquals(toBeExpected, charPositionsInString)
    }

    @Test
    fun charPositionsInStringTestButNoOccurrence() {
        val string = "provaaa"
        val char = 'h'
        val charPositionsInString = charPositionsInString(string,char)
        val toBeExpected = listOf<Int>()
        assertEquals(toBeExpected, charPositionsInString)
    }


    @Test
    fun firstOccurrencePositionTest() {
        val string = "provaaa"
        val char = 'o'
        val firstOccurrencePosition = firstOccurrencePosition(string, char)
        val toBeExpected = 2
        assertEquals(toBeExpected, firstOccurrencePosition)
    }

    @Test
    fun firstOccurrencePositionTestButNoOccurrence() {
        val string = "provaaa"
        val char = 'h'
        val firstOccurrencePosition = firstOccurrencePosition(string, char)
        val toBeExpected = 2
        assertEquals(toBeExpected, firstOccurrencePosition)
    }

    @Test
    fun firstOccurrencePositionTestButWrongPosition() {
        val string = "provaaa"
        val char = 'p'
        val firstOccurrencePosition = firstOccurrencePosition(string, char)
        val toBeExpected = 3
        assertEquals(toBeExpected, firstOccurrencePosition)
    }

    @Test
    fun roundOffTestButUp() {
        val grade = 5.8
        val roundOff = roundOff(grade)
        val toBeExpected = 6
        assertEquals(toBeExpected, roundOff)
    }

    @Test
    fun roundOffTest() {
        val grade = 5.8
        val roundOff = roundOff(grade)
        val toBeExpected = 5
        assertEquals(toBeExpected, roundOff)
    }

    @Test
    fun roundUpTestBelow5() {
        val grade = 4.8
        val roundOff = roundOff(grade)
        val toBeExpected = 5
        assertEquals(toBeExpected, roundOff)
    }

    @Test
    fun roundOffTestBelow5() {
        val grade = 4.8
        val roundOff = roundOff(grade)
        val toBeExpected = 4
        assertEquals(toBeExpected, roundOff)
    }
}